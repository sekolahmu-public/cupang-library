module gitlab.com/sekolahmu-public/cupang-library

go 1.15

require (
	github.com/Shopify/sarama v1.32.0
	github.com/devfeel/mapper v0.7.10 // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/google/uuid v1.3.0
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/newrelic/go-agent/v3 v3.15.2 // indirect
	github.com/newrelic/go-agent/v3/integrations/nrgin v1.1.2 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0
	go.elastic.co/apm v1.15.0
	go.elastic.co/apm/module/apmgin v1.15.0
	golang.org/x/crypto v0.0.0-20220307211146-efcb8507fb70 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	gorm.io/gorm v1.23.4 // indirect
)
