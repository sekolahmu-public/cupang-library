package logger

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func callAPI(httpMethod string, url string, jsonData interface{}, headers map[string]string, queryParams map[string]string) (*http.Response, error) {
	var request *http.Request
	jsonValue, _ := json.Marshal(jsonData)
	request, _ = http.NewRequest(httpMethod, url, bytes.NewBuffer(jsonValue))

	request.Header.Set("Content-Type", "application/json")
	for key, val := range headers {
		request.Header.Add(key, val)
	}
	client := &http.Client{}
	response, err := client.Do(request)

	return response, err
}
