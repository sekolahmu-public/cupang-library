package logger

import (
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type logTestCase struct {
	name                    string
	level                   Level
	data                    interface{}
	expectedLevel           string
	expectedAdditionalInfo0 map[string]interface{}
	expectedAdditionalInfo1 map[string]interface{}
	hasNoTrace              bool
	oldData                 interface{}
	newData                 interface{}
	expectedIP              string
	expectedForwardedIP     string
}

type objLogTest struct {
	ID        int
	Name      string
	Timestamp time.Time
	Data      []string
}

type MockFileLog struct {
	mock.Mock
}

var (
	logFileSetting = &LogFileSetting{
		WriteLog:    true,
		LogDir:      "/log",
		FileMaxSize: 10,
	}

	kafkaSetting = &KafkaStreamSetting{
		StreamLog:            false,
		KafkaBootstrapServer: "KafkaServer",
	}

	apmConfig = &APMConfig{
		Router: gin.Default(),
	}

	apiSetting = &APISetting{
		SendLog:   true,
		ApiUrl:    "http://log",
		AuthToken: "auth-token",
	}
)

func TestInitLogger(t *testing.T) {
	var (
		serviceName string = "Service Test"
	)

	logFileSetting := &LogFileSetting{
		WriteLog:    true,
		LogDir:      "/log",
		FileMaxSize: 10,
	}

	kafkaSetting := &KafkaStreamSetting{
		StreamLog:            false,
		KafkaBootstrapServer: "KafkaServer",
	}

	apmConfig = &APMConfig{
		Router: gin.Default(),
	}

	apiSetting := &APISetting{}

	logger := Open(serviceName, true, logFileSetting, kafkaSetting, apiSetting, apmConfig)
	assert.Equal(t, serviceName, logger.ServiceName)

}

func TestLog(t *testing.T) {
	serviceName := "Service Test"
	timeNow := time.Now()
	logger := Open(serviceName, true, logFileSetting, kafkaSetting, apiSetting, apmConfig)
	//logger1 := Open(serviceName, false, false, "", "")
	//logger2 := Open(serviceName, false, true, "http://api.sekolah.mu", "Auth-Token")
	//logger3 := Open(serviceName, true, true, "http://api.sekolah.mu", "Auth-Token")

	testCases := []logTestCase{
		{
			name:                    "Info string data should not return funtion name and trace",
			level:                   Info,
			data:                    "This is info",
			expectedLevel:           "info",
			expectedAdditionalInfo0: map[string]interface{}{"var_0": "This is info"},
			expectedAdditionalInfo1: map[string]interface{}{"var_0": "This is info"},
			hasNoTrace:              true,
			expectedIP:              "10.0.0.1",
			expectedForwardedIP:     "10.0.0.2",
		},
		{
			name:                    "Info object data should not return funtion name and trace",
			level:                   Info,
			data:                    objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}},
			expectedLevel:           "info",
			expectedAdditionalInfo0: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			expectedAdditionalInfo1: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			hasNoTrace:              true,
			expectedIP:              "10.0.0.1",
			expectedForwardedIP:     "10.0.0.2",
		},
		{
			name:                    "Error object data should not return funtion name and trace",
			level:                   Error,
			data:                    objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}},
			expectedLevel:           "error",
			expectedAdditionalInfo0: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			expectedAdditionalInfo1: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			hasNoTrace:              false,
			expectedIP:              "10.0.0.1",
			expectedForwardedIP:     "10.0.0.2",
		},
		{
			name:                    "Warning object data should not return funtion name and trace",
			level:                   Warning,
			data:                    objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}},
			expectedLevel:           "warning",
			expectedAdditionalInfo0: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			expectedAdditionalInfo1: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			hasNoTrace:              true,
			expectedIP:              "10.0.0.1",
			expectedForwardedIP:     "10.0.0.2",
		},
		{
			name:                    "Warning object data should not return funtion name and trace",
			level:                   Debug,
			data:                    objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}},
			expectedLevel:           "debug",
			expectedAdditionalInfo0: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			expectedAdditionalInfo1: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			hasNoTrace:              false,
			expectedIP:              "10.0.0.1",
			expectedForwardedIP:     "10.0.0.2",
		},
		{
			name:                    "Error object data should not return funtion name and trace and send log",
			level:                   Debug,
			data:                    objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}},
			expectedLevel:           "debug",
			expectedAdditionalInfo0: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			expectedAdditionalInfo1: map[string]interface{}{"obj_0": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "log"}}},
			hasNoTrace:              false,
			expectedIP:              "10.0.0.1",
			expectedForwardedIP:     "10.0.0.2",
		},
	}

	for i := 1; i < 2; i++ {
		for _, tc := range testCases {
			t.Run(tc.name, func(t *testing.T) {
				correlationID := uuid.New().String()
				if i == 1 {
					logInfo := logger.Log(correlationID, tc.level, nil, tc.data)
					assert.Equal(t, correlationID, logInfo.CorrelationID)
					assert.Equal(t, tc.expectedLevel, logInfo.Level)
					assert.Equal(t, tc.expectedAdditionalInfo0, logInfo.AdditionalInfo)
					assert.Equal(t, tc.hasNoTrace, logInfo.Trace == "")
				}
			})

		}

	}

}

func TestAuditLog(t *testing.T) {
	serviceName := "Service Test"
	timeNow := time.Now()
	logger := Open(serviceName, true, logFileSetting, kafkaSetting, apiSetting, apmConfig)
	//logger1 := Open(serviceName, false, false, "", "")
	//logger2 := Open(serviceName, false, true, "http://api.sekolah.mu", "Auth-Token")
	//logger3 := Open(serviceName, true, true, "http://api.sekolah.mu", "Auth-Token")

	testCases := []logTestCase{
		{
			name:                    "Info string data should not return funtion name and trace",
			oldData:                 "This is old data",
			newData:                 "This is new data",
			expectedLevel:           "audit",
			expectedAdditionalInfo0: map[string]interface{}{"old_data": "This is old data", "new_data": "This is new data"},
			hasNoTrace:              true,
		},
		{
			name:                    "Info object data should not return funtion name and trace",
			oldData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}},
			newData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}},
			expectedLevel:           "audit",
			expectedAdditionalInfo0: map[string]interface{}{"old_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}}, "new_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}}},
			hasNoTrace:              true,
		},
		{
			name:                    "Error object data should not return funtion name and trace",
			oldData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}},
			newData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}},
			expectedLevel:           "audit",
			expectedAdditionalInfo0: map[string]interface{}{"old_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}}, "new_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}}},
			hasNoTrace:              false,
		},
		{
			name:                    "Warning object data should not return funtion name and trace",
			oldData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}},
			newData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}},
			expectedLevel:           "audit",
			expectedAdditionalInfo0: map[string]interface{}{"old_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}}, "new_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}}},
			hasNoTrace:              true,
		},
		{
			name:                    "Warning object data should not return funtion name and trace",
			oldData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}},
			newData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}},
			expectedLevel:           "audit",
			expectedAdditionalInfo0: map[string]interface{}{"old_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}}, "new_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}}},
			hasNoTrace:              false,
		},
		{
			name:                    "Error object data should not return funtion name and trace and send log",
			oldData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}},
			newData:                 objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}},
			expectedLevel:           "audit",
			expectedAdditionalInfo0: map[string]interface{}{"old_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "old"}}, "new_data": objLogTest{ID: 1, Name: "log", Timestamp: timeNow, Data: []string{"This", "is", "new"}}},
			hasNoTrace:              false,
		},
	}

	for i := 1; i < 2; i++ {
		for _, tc := range testCases {
			t.Run(tc.name, func(t *testing.T) {
				correlationID := uuid.New().String()
				if i == 1 {
					logInfo := logger.AuditLog(correlationID, tc.oldData, tc.newData)
					assert.Equal(t, correlationID, logInfo.CorrelationID)
					assert.Equal(t, tc.expectedLevel, logInfo.Level)
					assert.Equal(t, tc.expectedAdditionalInfo0, logInfo.AdditionalInfo)
				}
			})

		}

	}

}
