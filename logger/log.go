package logger

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"runtime"
	"runtime/debug"
	"strings"
	"time"

	"github.com/newrelic/go-agent/v3/integrations/nrgin"
	"github.com/newrelic/go-agent/v3/newrelic"
	"go.elastic.co/apm/module/apmgin"
)

type Level uint32

const (
	Info Level = iota
	Warning
	Error
	Debug
)

type ILog interface {
	Log(level Level, args ...interface{}) *LogInfo
}

func Open(serviceName string, printLog bool, logFileSetting *LogFileSetting,
	kafkaStreamSetting *KafkaStreamSetting, apiSetting *APISetting, apmConfig *APMConfig) *CupangSetting {

	if !isNil(logFileSetting) {
		// pending implementation
		fmt.Sprintln("Not implemented error")

		// err = os.MkdirAll(logFileSetting.LogDir, 0755)
		// if err != nil {
		// 	log.Fatal(err)
		// }
		// if len(logFileSetting.FileNamePrefix) == 0 {
		// 	logFileSetting.FileNamePrefix = fmt.Sprintf("%s_log_", serviceName)
		// }
		// if logFileSetting.FileMaxSize == 0 {
		// 	logFileSetting.FileMaxSize = 100 * 1024 * 1024
		// } else {
		// 	logFileSetting.FileMaxSize = logFileSetting.FileMaxSize * 1024 * 1024
		// }
	}

	if !isNil(kafkaStreamSetting) && kafkaStreamSetting.StreamLog {
		if len(kafkaStreamSetting.LogTopicName) == 0 {
			kafkaStreamSetting.LogTopicName = fmt.Sprintf("Log_%s", serviceName)
		}
		kafkaStreamSetting.kafkaConnector = InitKafkaRepository(
			kafkaStreamSetting.KafkaBootstrapServer,
			kafkaStreamSetting.LogTopicName,
		)

	}

	if !isNil(apmConfig) {
		apmConfig.Router.Use(apmgin.Middleware(apmConfig.Router))
		if !isNil(apmConfig.NewRelic) {
			app, err := newrelic.NewApplication(
				newrelic.ConfigAppName(apmConfig.AppName),
				newrelic.ConfigLicense(apmConfig.NewRelic.NewRelicLicense),
				newrelic.ConfigEnabled(true),
			)
			if err != nil {
				log.Fatal(err)
			}
			apmConfig.Router.Use(nrgin.Middleware(app))
			app.WaitForConnection(5 * time.Second)
			apmConfig.NewRelic.App = app
		}
	}

	setting := CupangSetting{
		ServiceName:        serviceName,
		PrintLog:           printLog,
		APISetting:         apiSetting,
		LogFileSetting:     logFileSetting,
		KafkaStreamSetting: kafkaStreamSetting,
		APMConfig:          apmConfig,
	}

	return &setting
}

func (logger *CupangSetting) Log(correlationID string, level Level, args ...interface{}) *LogInfo {
	var (
		lineDetails  string
		functionName string
		info         map[string]interface{} = make(map[string]interface{})
		trace        string
		ip           string
		forwardedIP  string
	)
	levelString, _ := getLevelString(level)
	complexType := map[reflect.Kind]bool{
		reflect.Struct:     true,
		reflect.Complex128: true,
		reflect.Complex64:  true,
	}
	pc, file, line, ok := runtime.Caller(1)
	details := runtime.FuncForPC(pc)
	if ok && details != nil && (level == Error || level == Debug || level == Warning) {
		lineDetails = fmt.Sprintf("File: %s\n, line: %d\n, function: %s\n", file, line, details.Name())
		functionName = details.Name()
	}

	for i := range args {
		idxVar := 0
		if i == 0 &&
			reflect.ValueOf(args[i]).Kind() == reflect.String &&
			strings.HasPrefix(args[i].(string), "endpoint:") {
			functionName = args[i].(string)[9:]
		} else if i > 0 &&
			reflect.ValueOf(args[i]).Kind() == reflect.String &&
			strings.HasPrefix(args[i].(string), "ip:") {
			ip = args[i].(string)[3:]
		} else if i > 0 &&
			reflect.ValueOf(args[i]).Kind() == reflect.String &&
			strings.HasPrefix(args[i].(string), "forwarded_ip:") {
			forwardedIP = args[i].(string)[13:]
		} else {
			infoKey := fmt.Sprintf("var_%d", idxVar)
			if complexType[reflect.TypeOf(args[i]).Kind()] {
				infoKey = fmt.Sprintf("obj_%d", idxVar)
			}
			info[infoKey] = args[i]
			idxVar++
		}

	}

	if level == Error || level == Debug {
		trace = string(debug.Stack())
	}

	logInfo := &LogInfo{
		CorrelationID:  correlationID,
		Level:          levelString,
		Timestamp:      time.Now(),
		ServiceName:    logger.ServiceName,
		FunctionName:   functionName,
		LineDetails:    lineDetails,
		AdditionalInfo: info,
		Trace:          trace,
		IP:             ip,
		ForwardedIP:    forwardedIP,
	}

	if logger.PrintLog {
		infoByte, _ := json.Marshal(logInfo)
		log.Print(string(infoByte))
	}

	if !isNil(logger.APISetting) && logger.SendLog {
		resp, _ := callAPI("POST", logger.ApiUrl, logInfo,
			map[string]string{"Authorization": logger.AuthToken}, nil)
		if !isNil(resp) {
			defer resp.Body.Close()
		}

	}

	if !isNil(logger.KafkaStreamSetting) && logger.StreamLog {
		message, err := json.Marshal(logInfo)
		if err != nil {
			fmt.Print(err)
		}
		logger.kafkaConnector.ProduceMessage(correlationID, message)
	}
	// LogFileSetting not implemented yet

	return logInfo
}

func (logger *CupangSetting) AuditLog(correlationID string, oldData interface{}, newData interface{}) *LogInfo {
	var (
		functionDetails string
		info            map[string]interface{} = make(map[string]interface{})
	)
	levelString := "audit"
	pc, file, line, ok := runtime.Caller(1)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		functionDetails = fmt.Sprintf("File: %s\n, line: %d\n, function: %s\n", file, line, details.Name())
	}
	info["old_data"] = oldData
	info["new_data"] = newData

	logInfo := &LogInfo{
		CorrelationID:  correlationID,
		Level:          levelString,
		Timestamp:      time.Now(),
		ServiceName:    logger.ServiceName,
		FunctionName:   functionDetails,
		AdditionalInfo: info,
	}

	if logger.PrintLog {
		message, _ := json.Marshal(logInfo)
		log.Print(string(message))
	}

	if !isNil(logger.APISetting) && logger.SendLog {
		resp, _ := callAPI("POST", logger.ApiUrl, logInfo,
			map[string]string{"Authorization": logger.AuthToken}, nil)
		if !isNil(resp) {
			defer resp.Body.Close()
		}
	}

	if !isNil(logger.KafkaStreamSetting) && logger.StreamLog {
		message, err := json.Marshal(logInfo)
		if err != nil {
			fmt.Print(err)
			log.Print(err)
		}
		logger.kafkaConnector.ProduceMessage(correlationID, message)
	}

	return logInfo
}

func isNil(val interface{}) bool {
	return val == nil || reflect.ValueOf(val).IsNil() || (reflect.ValueOf(val).Kind() != reflect.Ptr && reflect.ValueOf(val).Len() == 0)
}

func getLevelString(level Level) (string, error) {
	switch level {
	case Info:
		return "info", nil
	case Warning:
		return "warning", nil
	case Error:
		return "error", nil
	case Debug:
		return "debug", nil
	}

	return "", fmt.Errorf("not a valid logrus Level: %q", level)
}
