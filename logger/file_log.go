package logger

import (
	"log"
	"os"
)

// development in progress

func createLogFile(path string) (*os.File, error) {
	logFile, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	return logFile, nil
}
