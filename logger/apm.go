package logger

import (
	"context"
	"fmt"

	"github.com/newrelic/go-agent/v3/newrelic"
	"go.elastic.co/apm"
)

func (logger *CupangSetting) TimeSpan(ctx context.Context, nrtxn interface{}, spanName string, spanType string) *SpanResponse {
	var resp SpanResponse
	resp.APMElasticObjResponse = &APMElasticObjResponse{}
	resp.NewRelicObjResponse = &NewRelicObjResponse{}

	if !isNil(logger.APMConfig) {
		//APM Elastic obj set
		if !isNil(logger.APMConfig.APMElastic) {
			span, ctx := apm.StartSpan(ctx, spanName, spanType)
			resp.APMElasticObjResponse.Span = span
			resp.APMElasticObjResponse.APMECtx = ctx
		}
		//NR object set
		if !isNil(logger.APMConfig.NewRelic) {
			txn := nrtxn.(*newrelic.Transaction)
			segment := txn.StartSegment(spanName)
			resp.NewRelicObjResponse.Segment = segment
		}
	}
	return &resp
}

func (logger *CupangSetting) ApmLog(err error, nrtxn interface{}, level Level) {
	if !isNil(logger.APMConfig) {
		strLevel, errLevel := getLevelString(level)
		errs := fmt.Errorf("%s-%s", strLevel, err)
		if errLevel != nil {
			errs = fmt.Errorf("%s-%s", errLevel.Error(), err)
		}

		//Untuk yang error
		if level == Error {
			//APM elastic
			if !isNil(logger.APMConfig.APMElastic) {
				e := apm.DefaultTracer.NewError(errs)
				e.Send()
			}
			//New relic
			if !isNil(logger.APMConfig.NewRelic) && !isNil(nrtxn) {
				txn := nrtxn.(*newrelic.Transaction)
				txn.NoticeError(errs)
			}
		}
	}

}

func (logger *CupangSetting) CupangEndSpan(span *SpanResponse) {
	if !isNil(span.APMElasticObjResponse.Span) {
		span.APMElasticObjResponse.Span.End()
	}
	if !isNil(span.NewRelicObjResponse.Segment) {
		span.NewRelicObjResponse.Segment.End()
	}
}

func (logger *CupangSetting) BuildContext(ctx context.Context, nrtxn interface{}) context.Context {
	txn := nrtxn.(*newrelic.Transaction)
	return newrelic.NewContext(context.Background(), txn)
}
