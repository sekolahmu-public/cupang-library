package logger

//region imports
import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/Shopify/sarama"
)

//endregion imports

//region structs

type Kafka struct {
	producer sarama.SyncProducer
	topic    string
}

//endregion structs

//region functions

func InitKafkaRepository(kafkaBootstrap string, logTopic string) *Kafka {
	var (
		kafkaConfig = getKafkaConfig("", "")
		kafkaBroker = kafkaBootstrap
		nodes       = strings.Split(kafkaBroker, ",")
	)
	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)

	kafkaProducer, err := sarama.NewSyncProducer(nodes, kafkaConfig)

	if err != nil {
		fmt.Print(err)
	}

	kafka := &Kafka{
		producer: kafkaProducer,
		topic:    logTopic,
	}

	return kafka

}

func getKafkaConfig(username, password string) *sarama.Config {
	kafkaConfig := sarama.NewConfig()
	kafkaConfig.Producer.Return.Successes = true
	kafkaConfig.Producer.Flush.Frequency = 500 * time.Millisecond
	kafkaConfig.Net.TLS.Enable = true

	if username != "" {
		kafkaConfig.Net.SASL.Enable = true
		kafkaConfig.Net.SASL.User = username
		kafkaConfig.Net.SASL.Password = password
	}
	return kafkaConfig
}

func (k *Kafka) ProduceMessage(key string, data []byte) error {
	message := sarama.ByteEncoder(data)

	keyMessage := sarama.StringEncoder(key)

	kafkaMessage := &sarama.ProducerMessage{
		Topic: k.topic,
		Value: &message,
		Key:   &keyMessage,
	}

	partition, offset, err := k.producer.SendMessage(kafkaMessage)
	if err != nil {
		fmt.Print(err)
		return err
	}
	fmt.Sprintf("Message successfully produced to topic %s, partition %d, offset %d", k.topic, partition, offset)
	return nil
}
