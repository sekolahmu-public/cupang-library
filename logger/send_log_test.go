package logger

import (
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockHttpClient struct {
	mock.Mock
}

func (m *MockHttpClient) Do(request *http.Request) (*http.Response, error) {
	args := m.Called(request)
	return args.Get(0).(*http.Response), args.Error(1)
}

func TestCallAPI(t *testing.T) {
	var (
		httpMethod string   = "POST"
		url        string   = "http://api.sekolah.mu"
		jsonData   *LogInfo = &LogInfo{
			CorrelationID:  uuid.New().String(),
			Level:          "info",
			Timestamp:      time.Now(),
			ServiceName:    "Service Test",
			FunctionName:   "Function Test",
			AdditionalInfo: map[string]interface{}{"var_0": "This is info"},
			Trace:          "",
		}
		headers map[string]string = map[string]string{"Authorization": "Auth-Token"}
	)
	resp, err := callAPI(httpMethod, url, jsonData, headers, nil)
	assert.NotNil(t, resp)
	assert.NoError(t, err)
}
