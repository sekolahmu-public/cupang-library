package logger

import (
	"context"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/newrelic/go-agent/v3/newrelic"
	"go.elastic.co/apm"
)

var (
	SpanSegmentNameKey       = "SpanSegmentName"
	LatestAPMSpanResponseKey = "LatestAPMSpanResponse"
)

type CupangSetting struct {
	ServiceName string
	PrintLog    bool
	*APISetting
	*LogFileSetting
	*KafkaStreamSetting
	*APMConfig
}

type APISetting struct {
	SendLog   bool
	ApiUrl    string
	AuthToken string
}

type LogFileSetting struct {
	WriteLog       bool
	LogDir         string
	FileMaxSize    int64
	FileNamePrefix string
	logFile        *os.File
}

type KafkaStreamSetting struct {
	KafkaBootstrapServer string
	StreamLog            bool
	LogTopicName         string
	kafkaConnector       *Kafka
}

type LogInfo struct {
	CorrelationID  string
	Level          string
	Timestamp      time.Time
	ServiceName    string
	FunctionName   string
	LineDetails    string
	AdditionalInfo map[string]interface{}
	Trace          string
	IP             string
	ForwardedIP    string
}

type APMConfig struct {
	Router     *gin.Engine
	AppName    string
	APMElastic *APMElasticSetting
	NewRelic   *NewRelicSetting
}

type APMElasticSetting struct {
	ServerUrl string
	AppEnv    string
}

type NewRelicSetting struct {
	NewRelicLicense string
	App             *newrelic.Application
}

type APMElasticObjRequest struct {
	Name     string
	SpanType string
	Ctx      context.Context
}

type APMElasticObjResponse struct {
	APMECtx context.Context
	Span    *apm.Span
}

//changes
type NewRelicObjRequest struct {
	SegmentName string
	Transaction *newrelic.Transaction
}

type NewRelicObjResponse struct {
	Segment *newrelic.Segment
}

type SpanResponse struct {
	*APMElasticObjResponse
	*NewRelicObjResponse
}

var (
	SpanTypeRpc          = "rpc"
	SpanTypeGoRoutine    = "goroutine"
	SpanTypeCrossService = "cross"
	SpanTypeController   = "controller"
	SpanTypeLogic        = "logic"
	SpanTypeRepo         = "repository"
	SpanTypeUtil         = "util"
)
